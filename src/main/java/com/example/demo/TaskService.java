package com.example.demo;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskService {
    private TaskRepository repository;
    public Page<Task> findTasks(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Optional<Task> findTask(Long id) {
        return repository.findById(id);
    }

    public Task createTask(Task task) {
        return repository.save(task);
    }

    public Task updateTask(Task task) {
        return repository.save(task);
    }
}
