package com.example.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
public class Task {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String description;
    private Boolean completed;
    private LocalDate createDate;
    private LocalDate completeDate;
}
