package com.example.demo;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/tasks")
public class TaskRestController {
    private final TaskService service;
    private final ModelMapper modelMapper;

    @GetMapping
    public Page<TaskDto> allTasks(@PageableDefault(size = 5) Pageable pageable) {
        return service.findTasks(pageable)
                .map(ent -> modelMapper.map(ent, TaskDto.class));
    }

    @GetMapping("/{id}")
    public Optional<TaskDto> singleTask(@PathVariable("id") Long id) {
        return service.findTask(id)
                .map(ent -> modelMapper.map(ent, TaskDto.class));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto createTask(@RequestBody TaskDto task) {
        task.setId(0L);
        Task result = service.createTask(modelMapper.map(task, Task.class));
        return modelMapper.map( result, TaskDto.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TaskDto updateTask(@PathVariable("id") Long id, @RequestBody TaskDto task) {
        task.setId(id);
        Task result= service.updateTask(modelMapper.map(task, Task.class));
        return modelMapper.map( result, TaskDto.class);
    }
}