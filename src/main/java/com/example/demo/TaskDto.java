package com.example.demo;

import lombok.Data;

@Data
public class TaskDto {
    private String title;
    private String description;
    private Boolean completed;
    private String createDate;
    private String completeDate;
    private Long id;
}
